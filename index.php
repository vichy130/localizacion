
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <title>Localización de vacunación</title>
</head>
<body>
    <div class="container">
		<div class="columns">
 
			<form action="./resultados.php" method="POST">
                    <!-- Codigo postal-->
                </br>
                <label class="form-label" for="input-text">Ingresa tu código postal</label>
                 </br>   
                <input name="codigo" class="form-input " type="text" id="codigo" placeholder="Código">
                    <!-- Codigo postal-->
                </br>
				<!-- Botones -->
				<input type='submit' class="btn" value="Enviar"/>
				<input type='reset' class="btn btn-primary" value="limpiar"/>
			</form>
		</div>
	</div>
</body>

</html>
